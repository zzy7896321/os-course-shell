OS Fall 2014 Assignment: Shell
================================

##Build and run
To build,
  
    make

To build in debug mode,

    make build=debug
    
To run the shell,

    ./shell
    
To clean,

    make clean
    
The shell was tested on Ubuntu 14.04 Server with GCC 4.9.1.

##Usage
At the beggining of each line, the shell will prompt "shell: ", at which point the shell is ready to accept inputs. Commands is in the following syntax:

- line ::= %empty | piped-command optional-background-specifier;

- optional-background-specifier ::= %empty | '&';

- piped-command ::= command | piped-command '|' command;

- command ::= arguments optional-input-redirection optional-output-redirection;

- arguments ::= STRING | arguments STRING;

- optional-input-redirection ::= %empty | '<' STRING;

- optional-output-redirection ::= %empty | '>' STRING;

where, STRING can be unquoted string with no space characters or ''/"" quoted string. \\, ' and " needs to be escaped with \\. If a \\ is followed by a character other than ' or ", both characters will be copied to the string verbatim. Quoted string can span across lines.

optional-input-redirection can only appear in the first command in a piped-command, while optional-output-direction can only appear in the last command in a piped-command. Violation to this limitation will incur the shell to report an error.

In addition, the shell aborts immediately on encountering an EOF.

If any error occurs during a system call, the shell reports the error and aborts any subsequent operations. The shell, however, will leave those processes that have started running. On error, the shell will wait until all the previously started processes in this command line terminate, regardless of whether the background specifier is present or not.


##Implementation
The shell consists of a hand-written parser, a command line interpreter.

In the main in shell.c, the shell ignores all the arguments, calls parse_init(stdin) and enter the read-and-eval loop. In each loop, the shell

1. Prints "shell: ".
2. Invokes parse_next_line() to read next line, the lexing result is stored in the global array parse_result with its length in parse_length.
3. The interpreter checks the semantics of the input and converts it into a job list. On error, the interpreter aborts the current line and continue the read-and-eval loop.
4. The job list is passed to fork_and_execute_jobs. The function establishes input/output redirections and pipes for the processes when forking and execute commands. If the command is a built-in command (e.g. cd), the process execute the command in the parent process rather than fork. If an & is present, the shell will prompt a message "[job_id] pid of the last process" indicating a background job is started. If an & is not present, the parent process blocks and waits for all the child processes started for this command line to terminate. Otherwise, fork_and_execute_jobs returns.
5. At the end of the read-and-eval loop, remaining background jobs are checked by invoking waitpid(the first pid of the job, 0, WNOHANG). If all processes of a job terminate, the job list is completely destroyed and a message "[job_id] done" is prompted.

Note:

- A built-in command must be the first command on a line and all following commands and the & specifier on the same line are ignored. 

- The only built-in command currently implemented is cd, which also ignores redirection of input. However, output of cd can be redirected. If no further argument is provided to cd, the it changes the working directory to $HOME. The first character of the argument is replaced with $HOME if it is ~.

- Files and pipes are opened only when needed. If creation of a file or a pipe fails, all the remaining of the line will be aborted. The shell waits until all processes on the command line started prior to the failure terminate. For instance, with command "./a | ./b | ./c &", the shell waits for ./a to terminate if the creation of the second pipe fails, and ./b is not even started. The read-end of the first pipe is closed so that ./a will terminate.

- When waiting for processes on a command line to terminate, the shell simply invokes waitpid on the pid of each process. When checking the background jobs, the shell invokes waitpid on the first pid in the job list. If the process did terminate, the first job is removed from the list and the next is checked. If not, the shell turns to the next job list and check it. To avoid getting blocked, the shell has to invoke waitpid with WNOHANG option enabled.

parser.c and parser.h implements a lexer that breaks input into tokens. The types of token can be found in token.h.
The read-and-eval-loop is implemented as mainloop() in exec.c and exec.h.
Other files contain utilities and miscellaneous stuff, like DUMP macros, DEBUG_OUTPUT macro, static assertion, built-in commands.

The parser does not impose any limit on the token length or the number of tokens on the same line. Strings of arbitary length is allowed as long as memory capability is not exceeded. However, the initial size of buffers are tailored to fit the size limit imposed in the assigment specification. They are automatically increased when a larger buffer is needed, but this functionality may be detrimetal to performance and is not fully tested.

