#ifndef OS_COURSE_SHELL_CONFIG_H
#define OS_COURSE_SHELL_CONFIG_H

/* in case of some compiler still does not support const */
#define CONST const

#if __STDC_VERSION__ >= 199901L
#	include <stdbool.h>
#	define BOOL bool
#	define TRUE true
#	define FALSE false
#else 
#	define BOOL int
#	define TRUE 1
#	define FALSE 0
#endif

/* constants */
#define MAX_TOKEN_LENGTH 32
#define MAX_LINE_LENGTH 512
#define MAX_TOKEN_NUMBER MAX_LINE_LENGTH

#define OS_SHELL_SUCCESS 0
#define OS_SHELL_IO_ERROR 1
#define OS_SHELL_OUT_OF_MEMORY 2
#define OS_SHELL_PARSE_ERROR 3

#define PIPEIN_FILENO 0
#define PIPEOUT_FILENO 1

#endif

