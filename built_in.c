#include "built_in.h"
#include "file_pipe.h"
#include "config.h"
#include "debug.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

DECL_BUILTIN(cd) {
	/* ignore redirection of input */
	FILE* out = stdout;
	if (out_file) {
		out = fopen_checked(out_file, "w");
		if (!out) return 1;
	}

	char* path;
	BOOL path_malloced = FALSE;
	if (argc == 1) {
		path = getenv("HOME");
		if (!path) {
			report_error(strerror(errno));
			return 1;
		}
	}
	else {
		path = argv[1];
		if (path[0] == '~') {
			char* home = getenv("HOME");
			if (!home) {
				report_error(strerror(errno));
			}
			int home_len = strlen(home);
			path = malloc(sizeof(char) * (home_len + strlen(argv[1]) - 1 + 1));
			if (!path) {
				report_error("out of memory");
			}
			else {
				path_malloced = TRUE;
			}
			strcpy(path, home);
			strcpy(path + home_len, argv[1] + 1);
		}
	}

	if (chdir(path)) {
		fprintf(out, "%s\n", strerror(errno));
	}

	if (out_file) fclose_checked(out);
	if (path_malloced) free(path);

	return 0;
}

