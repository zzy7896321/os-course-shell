#ifndef OS_COURSE_SHELL_PARSER_H
#define OS_COURSE_SHELL_PARSER_H

#include <stddef.h>
#include <stdio.h>

#include "config.h"
#include "token.h"

extern token_t**  parse_result;
extern size_t parse_length;
extern FILE* parse_input;

#define PARSE_EOLN (EOF - 1)

/**
 *	Initialize the parser.
 *
 *	@param file		the input, remember to close it yourself
 *	@returns		1
 */
int parse_init(FILE* file);

/**
 *	Parse the next line from parse_input.
 *	The result is stored in parse_result and parse_length.
 *
 *	@returns	TRUE on success, or FALSE on failure.
 */
BOOL parse_next_line();

/**
 *	Gets the next token from parse_input.
 *
 *	@returns	next token on success, or 0 if error occured.
 */
token_t* parse_next_token();

/**
 * Peek a character from the input. This operation may block.
 *
 * @returns	the next charater from the input without cursor advanced, or EOF if end-of-file encountered
 */
int parse_peek();

/**
 *	Gets a character from parse_input. This operation may block.
 *	@returns	the next character with cursor advanced by 1, or EOF if end-of-file encountered
 */
int parse_getc();

/**
 *	Peeks a character from parse_input. This operation never blocks.
 *	It may, however, fail when end-of-line is reached. Preceeding EOLN
 *	'\n' may or may not appear.
 *
 *	@returns	the next character on success, EOF if end-of-file is encountered,
 *				or PARSE_EOLN if end-of-line is encountered
 */
int parse_peek_nonblock();

#endif

