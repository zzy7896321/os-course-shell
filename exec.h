#ifndef OS_COURSE_SHELL_EXEC_H
#define OS_COURSE_SHELL_EXEC_H

#include "config.h"
#include "token.h"
#include <unistd.h>

typedef struct job_list_t job_list_t;
typedef struct job_t job_t;
typedef struct job_lists_t job_lists_t;

extern job_lists_t* background_jobs;

/**
 *	Enters the read-and-eval loop.
 *
 *	@returns	0 on success, or non-zero on failure
 */
int mainloop();

/**
 *	Fork and execute the jobs.
 *
 *	@param head		head of the job list
 *	@returns	always 0
 */
int fork_and_execute_jobs(job_list_t* head);

/**
 *	Fork and execute the job.
 *
 *	@returns	the pid, or -1 on failure
 */
pid_t fork_and_execute_job(job_t* job, int in_fd, int out_fd, int to_close);

/**
 *	Checks whether background jobs are finished.
 *
 *	@returns	always 0
 */
int check_for_background_jobs();

struct job_t {
	int argc;
	char** argv;
	char* in_file;
	char* out_file;
	BOOL use_pipe;
	BOOL background;

	int pid;
};

job_t* new_job();

void job_fill_argv(job_t* job, token_t* token[], int offset);

void job_destroy(job_t* job);

int dump_job(char* buffer, int buf_size, job_t* job);

struct job_list_t {
	job_t* job;
	job_list_t* next;
};

void job_list_clean(job_list_t* head);

struct job_lists_t {
	unsigned num;
	pid_t last_pid;
	job_list_t* job_list;
	job_lists_t* next;
};

job_lists_t* new_job_lists(job_list_t* job_list, job_lists_t* next, pid_t last_pid);

#endif

