#ifndef OS_COURSE_SHELL_FILE_PIPE_H
#define OS_COURSE_SHELL_FILE_PIPE_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>


FILE* fopen_checked(const char* path, const char* mode);
int fclose_checked(FILE* fp);

int open_checked(const char* path, int flags);
int close_checked(int fd);

int pipe_checked(int pipefd[2]);

#endif

