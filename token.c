#include "token.h"

#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#include "debug.h"

CONST char* token_names[NUMBER_OF_TOKEN] = {
	"TOKEN_INVALID",
	"TOKEN_REDIR_IN",
	"TOKEN_REDIR_OUT",
	"TOKEN_PIPE_IO",
	"TOKEN_BACKGROUND",
	"TOKEN_STR",
	"TOKEN_EOLN",
	"TOKEN_EOF",
};

token_t* new_token(token_type_t type) {
	token_t* token = malloc(sizeof(token_t));
	token->token_type = type;
	return token;
}

token_t* new_token_str_transfer(char* str) {
	token_str_t* token_str = malloc(sizeof(token_str_t));
	token_str->super.token_type = TOKEN_STR;
	token_str->content = str;
	
	return (token_t*) token_str;
}

token_t* new_token_str_duplicate(CONST char* str, int offset, size_t len) {
	token_str_t* token_str = malloc(sizeof(token_str_t));
	token_str->super.token_type = TOKEN_STR;
	token_str->content = malloc(sizeof(char) * len);
	strncpy(token_str->content, str + offset, len);

	return (token_t*) token_str;
}

char* token_str_transfer_string(token_str_t* token) {
	char* ret = token->content;
	token->content = 0;
	return ret;
}

static void token_destroy_impl(token_t* token) {
	/* no check for NULL */
	if (token->token_type == TOKEN_STR && TOKEN_STR_OF(token)) {
		free(TOKEN_STR_OF(token));
	}
	free(token);
}

void token_destroy(token_t* token) {
	if (token) token_destroy_impl(token);	
}

void token_destroy_arr(token_t* token_arr[], size_t length) {
	while (length != 0) {
		if (token_arr[--length]) {
			token_destroy_impl(token_arr[length]);
			token_arr[length] = 0;
		}
	}
}

int dump_token(char* buffer, int buf_size, token_t* token) {
	DUMP_START();
	if (token->token_type < 0 || token->token_type >= NUMBER_OF_TOKEN) {
		DUMP(buffer, buf_size, token_names[TOKEN_INVALID]);
	}
	else {
		DUMP(buffer, buf_size, token_names[token->token_type]);
	}

	if (token->token_type == TOKEN_STR) {
		DUMP(buffer, buf_size, "(%s)", TOKEN_STR_OF(token));
	}

	DUMP_SUCCESS();
}

