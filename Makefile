build ?= release
CC ?= gcc
ifeq ($(build),debug)
CFLAGS += -Wall -g
else
CFLAGS += -Wall -O2 -DNDEBUG
endif

OBJS = $(patsubst %.c,%.o,$(wildcard *.c))
HEADERS = $(wildcard *.h)

all: shell

shell: $(OBJS)
	echo $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^

%.o: %.c $(HEADERS)
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	rm -f $(OBJS) shell

.PHONY: all clean

