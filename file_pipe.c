#include "file_pipe.h"
#include "debug.h"
#include <errno.h>
#include <string.h>

FILE* fopen_checked(const char* path, const char* mode) {
	FILE* file = fopen(path, mode);
	if (!file) {
		report_error(strerror(errno));
	}

	return file;
}

int fclose_checked(FILE* fp) {
	if (fclose(fp)) {
		report_error(strerror(errno));
		return 0;
	}

	return 1;
}

int open_checked(const char* path, int flags) {
	mode_t mode = 0664;
	int fd = open(path, flags, mode);
	if (fd < 0) {
		report_error(strerror(errno));
	}

	return fd;
}

int close_checked(int fd) {
	if (close(fd)) {
		report_error(strerror(errno));
		return 0;
	}
	return 1;
}

int pipe_checked(int pipefd[2]) {
	if (pipe(pipefd)) {
		report_error(strerror(errno));
		pipefd[0] = pipefd[1] = -1;
		return 0;
	}

	return 1;
}

