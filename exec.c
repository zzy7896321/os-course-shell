#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <assert.h>

#include "exec.h"
#include "parser.h"
#include "debug.h"

#include "file_pipe.h"
#include "built_in.h"

#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

job_lists_t* background_jobs = 0;

#ifndef NDEBUG 
#	define DUMP_BUFFER_SIZE 8000
	static char dump_buffer[DUMP_BUFFER_SIZE];
#endif

#ifndef NDEBUG
/* for debugging */
static BOOL check_jobs(job_list_t* head, job_list_t* tail) {

	if (!head) {
		assert(!tail);
		return TRUE;
	}

	assert(!tail->job->use_pipe);
	if (head != tail) {
		assert(!tail->job->in_file);
	}

	while (head) {
		dump_job(dump_buffer, DUMP_BUFFER_SIZE, head->job);
		DEBUG_OUTPUT("job: %s", dump_buffer);

		assert(head->job->argc > 0);
		assert(head->job->argv);
		if (head->next) {
			assert(!head->job->out_file);
			assert(head->job->use_pipe);
			assert(!head->job->background);
		}

		else {
			assert(head == tail);
		}
		head = head->next;
	}

	return TRUE;
}
#endif

int mainloop() {
	static char token_buffer[MAX_TOKEN_LENGTH + 1 + 20];
	

	#define MAINLOOP_ABORT	\
		goto mainloop_abort
	#define REPORT_SYNTAX_ERROR(msg)	\
		do {	\
			int num_written = dump_token(token_buffer, MAX_TOKEN_LENGTH + 1 + 20, parse_result[i]);	\
			if (num_written > MAX_TOKEN_LENGTH + 20) {	\
				char* buffer = malloc(num_written + 1);	\
				dump_token(buffer, num_written + 1, parse_result[i]);	\
				report_error(msg " at token %u: %s", i, buffer);	\
				free(buffer);	\
			}	\
			else {	\
				report_error(msg " at token %u: %s", i, token_buffer);	\
			}	\
			MAINLOOP_ABORT;	\
		} while (0)

	while (TRUE) {
		printf("shell: ");
		fflush(stdout);
		if (!parse_next_line()) continue;
		assert(parse_length > 0);
		
		if (TOKEN_TYPE_OF(parse_result[parse_length - 1]) == TOKEN_EOF) {
			DEBUG_OUTPUT("<EOF>");
			printf("\n");
			fflush(stdout);
			return OS_SHELL_SUCCESS;
		}

		{
			size_t i;
			int first_arg = 0;
			job_list_t *head = 0, *tail = 0;
			job_list_t **tail_next_ptr = &head;
			job_t* now = 0;
			
			enum {EXEC_STATE_INITIAL, EXEC_STATE_REDIR_IN, EXEC_STATE_REDIR_OUT} 
				state = EXEC_STATE_INITIAL;
			#ifndef NDEBUG
				const char* state_name[] = {
					"INITIAL",
					"REDIR_IN",
					"REDIR_OUT",
				};
			#endif

			for ( i = 0; i != parse_length; ++i) {
				#ifndef NDEBUG
					dump_token(token_buffer, MAX_TOKEN_LENGTH + 21, parse_result[i]);
					DEBUG_OUTPUT("token %u, %s, state = %s", i, token_buffer, state_name[state]);
				#endif
				switch (state) {
				case EXEC_STATE_INITIAL:

					switch (TOKEN_TYPE_OF(parse_result[i])) {
					case TOKEN_EOF:
					case TOKEN_EOLN:
						{
							if (now) {
								job_fill_argv(now, parse_result, first_arg);
								now = 0;
							}

							if (!head) {
								MAINLOOP_ABORT;
							}

							/* check the tail does not pipe to the next,
							 * make sure that report this before check_jobs assertion*/
							if (tail->job->use_pipe) {
								REPORT_SYNTAX_ERROR("expecting more after |");
							}

							assert(check_jobs(head, tail));
							
							fork_and_execute_jobs(head);
							head = 0;	// otherwise, head will be re-freed
						}
						break;
					case TOKEN_STR:
						if (!now) {
							first_arg = (int) i;
							now = new_job();
							job_list_t* list_tail = malloc(sizeof(job_list_t));
							if (!list_tail) {
								/* free the resources */
								report_fatal_error(OS_SHELL_OUT_OF_MEMORY, "cannot allocate space for job list");
							}
							list_tail->job = now;
							list_tail->next = 0;
							tail = list_tail;
							*tail_next_ptr = list_tail;
							tail_next_ptr = &(list_tail->next);
						}
						++(now->argc);
						break;
					case TOKEN_REDIR_IN:
						if (!now || now->in_file || now != head->job) {
							/* 
							 *	1. < before occurence of any string
							 *	2. already specified redirection of stdin
							 *	3. trying to redirecting stdin of that uses a pipe
							 * */
							REPORT_SYNTAX_ERROR("unexpected <");
						}
						else {
							state = EXEC_STATE_REDIR_IN;
						}
						break;
	
					case TOKEN_REDIR_OUT:
						if (!now || now->out_file) {
							/* 
							 *	1. > before occurence of any string
							 *	2. already specified redirection of stdout
							 * */
							REPORT_SYNTAX_ERROR("unexpected >");
						}
						else {
							state = EXEC_STATE_REDIR_OUT;
						}
						break;
	
					case TOKEN_PIPE_IO:
						if (!now || now->out_file) {
							/*
							 *	1. | brefore occurence of any string
							 *	2. already specified redirection of stdout
							 */
							REPORT_SYNTAX_ERROR("unexpected |");
						}
						else {
							now->use_pipe = TRUE;
							job_fill_argv(now, parse_result, first_arg);
							now = 0;
						}
						break;
		
					case TOKEN_BACKGROUND:
						if (i + 2 != parse_length) {
							REPORT_SYNTAX_ERROR("unexpected &");
						}
						else {
							if (!now) {
								REPORT_SYNTAX_ERROR("unexpected &");
							}
							
							if (now->argc == 0) REPORT_SYNTAX_ERROR("unexpected &");
							now->background = TRUE;
						}
						break;
						
					default:
						assert(FALSE);
					}
					break;
				
				case EXEC_STATE_REDIR_IN:
					if (TOKEN_TYPE_OF(parse_result[i]) == TOKEN_STR) {
						now->in_file = token_str_transfer_string((token_str_t*) parse_result[i]);	
						state = EXEC_STATE_INITIAL;
					}
					else {
						REPORT_SYNTAX_ERROR("expecting string, but found");
					}
					break;
				
				case EXEC_STATE_REDIR_OUT:
					if (TOKEN_TYPE_OF(parse_result[i]) == TOKEN_STR) {
						now->out_file = token_str_transfer_string((token_str_t*) parse_result[i]);
						state = EXEC_STATE_INITIAL;
					}
					else {
						REPORT_SYNTAX_ERROR("expecting string, but found");
					}
					break;
					
				default:
					assert(FALSE);
				}

			}
		mainloop_abort:
			job_list_clean(head);

			check_for_background_jobs();	

		}
	}

	#undef REPORT_SYNTAX_ERROR
	#undef MAINLOOP_ABORT
}

int fork_and_execute_jobs(job_list_t* head) {
	if (!head) return 0;
	

	#define CHECK_AND_CALL_BUILTIN(name)	\
		do {	\
		if (!strcmp(head->job->argv[0], #name)) {	\
			CALL_BUILTIN(name, head->job->argc, head->job->argv, head->job->in_file, head->job->out_file);	\
			job_list_clean(head);	\
			return 0;	\
		} } while(0)

	CHECK_AND_CALL_BUILTIN(cd);
	#undef CHECK_AND_CALL_BUILTIN

	#define FORK_AND_EXECUTE_JOBS_ABORT\
		do {	\
			DEBUG_OUTPUT("FORK_AND_EXECUTE_JOBS_ABORT");	\
			goto fork_and_execute_jobs_abort;	\
		} while(0)
	{

	BOOL background = FALSE;
	job_list_t* prev = 0;
	job_list_t* now = head;
	pid_t pid = -1;

	int pipes[2] = {-1, -1};
	if (head->job->in_file) {
		int fd = open_checked(head->job->in_file, O_RDONLY);
		if (fd < 0) {
			FORK_AND_EXECUTE_JOBS_ABORT;
		}
		pipes[PIPEIN_FILENO] = fd;
	}
	else {
		pipes[PIPEIN_FILENO] = STDIN_FILENO;
	}

	while (now) {
		int in_fd = pipes[PIPEIN_FILENO];
		int out_fd = STDOUT_FILENO;
		assert(pipes[PIPEOUT_FILENO] == -1);

		pipes[0] = pipes[1] = -1;

		if (now->next) {
			if (!pipe_checked(pipes)) {
				if (in_fd > 2) {
					close_checked(in_fd);
				}
				
				FORK_AND_EXECUTE_JOBS_ABORT;
			}	
			out_fd = pipes[PIPEOUT_FILENO];
		}

		else {
			if (now->job->out_file) {
				int fd = open_checked(now->job->out_file, O_WRONLY | O_CREAT | O_TRUNC);
				if (fd < 0) {
					if (in_fd > 2) {
						close_checked(in_fd);
					}

					FORK_AND_EXECUTE_JOBS_ABORT;
				}
				out_fd = fd;	
			}
			background = now->job->background;
		}
		
		pid = fork_and_execute_job(now->job, in_fd, out_fd, pipes[PIPEIN_FILENO]);
		if (pid < 0) {
			background = FALSE;	// force waiting
			if (prev) {
				pid = prev->job->pid;
				prev->next = 0;
			}
			else {
				head = 0;
			}
			job_list_clean(now);
			FORK_AND_EXECUTE_JOBS_ABORT;
		}

		pipes[PIPEOUT_FILENO] = -1;

		prev = now;
		now = now->next;
	}

fork_and_execute_jobs_abort:
	if (head) {
		if (background) {
			background_jobs = new_job_lists(head, background_jobs, pid);	
			printf("[%u] %ld\n", background_jobs->num, (long) background_jobs->last_pid);
		}
		
		else {
			while (head) {
				assert(head->job->pid != -1);
				if (waitpid(head->job->pid, 0, 0) != head->job->pid) {
					report_error(strerror(errno));
				}

				job_list_t* to_destroy = head;
				head = head->next;
				job_destroy(to_destroy->job);
				free(to_destroy);
			}
		}
	}
	}

	#undef FORK_AND_EXECUTE_JOBS_ABORT

	return 0;
}

pid_t fork_and_execute_job(job_t* job, int in_fd, int out_fd, int to_close) {
#ifndef NDEBUG
	dump_job(dump_buffer, DUMP_BUFFER_SIZE, job);
	DEBUG_OUTPUT("execute job: %s, in_fd = %d, out_fd = %d, to_close = %d", dump_buffer, in_fd, out_fd, to_close);
#endif

	pid_t cpid = fork();

	if (cpid < 0) {
		report_error(strerror(errno));
		if (in_fd > 2) {
			close_checked(in_fd);
		}
		
		if (out_fd > 2) {
			close_checked(out_fd);
		}
		return cpid;
	}

	else if (cpid == 0) {
		if (to_close > 2) {
			close_checked(to_close);
		}

		if (in_fd > 2) {
			if (dup2(in_fd, STDIN_FILENO) == -1) {
				report_error(strerror(errno));
				return -1;
			}
			close_checked(in_fd);
		}

		if (out_fd > 2) {
			if (dup2(out_fd, STDOUT_FILENO) == -1) {
				report_error(strerror(errno));
				return -1;
				
			}
			close_checked(out_fd);
		}

		if (execvp(job->argv[0], job->argv) == -1) {
			report_fatal_error(1, strerror(errno));
		}
	}

	else {
		DEBUG_OUTPUT("pid = %ld", (long) cpid);
		if (in_fd > 2) {
			close_checked(in_fd);
		}
		
		if (out_fd > 2) {
			close_checked(out_fd);
		}

		job->pid = cpid;	
		return cpid;
	}

	assert(FALSE);
	return -1;
}

int check_for_background_jobs() {
	job_lists_t** prev_next_ptr = &background_jobs;
	job_lists_t* now = background_jobs;

	while (now) {
		pid_t ret;
		job_list_t* job_list = now->job_list;
		while (job_list && (ret = waitpid(job_list->job->pid, 0, WNOHANG))) {
			if (ret == job_list->job->pid)	{
				job_list_t* to_destroy = job_list;
				job_list = job_list->next;
				job_destroy(to_destroy->job);
				free(to_destroy);
			}

			else {
				report_error(strerror(errno));
			}
		}

		if (!job_list) {
			printf("[%u] Done\n", now->num);

			*prev_next_ptr = now->next;
			free(now);
			now = *prev_next_ptr;
		}
		else {
			now->job_list = job_list;
			prev_next_ptr = &(now->next);
			now = now->next;
		}
	}

	return 0;
}

job_t* new_job() {
	job_t* job = malloc(sizeof(job_t));
	if (!job) {
		report_fatal_error(OS_SHELL_OUT_OF_MEMORY, "cannot allocate space for job");
	}

	job->argc = 0;
	job->argv = 0;
	job->in_file = 0;
	job->out_file = 0;
	job->use_pipe = FALSE;
	job->background = FALSE;

	job->pid = -1;

	return job;
}

void job_fill_argv(job_t* job, token_t* token[], int offset) {
	int i;
	assert(job->argc > 0);
	job->argv = malloc(sizeof(char*) * (job->argc + 1));
	if (!job->argv) {
		report_fatal_error(OS_SHELL_OUT_OF_MEMORY, "cannot allocate space for command arguments");
	}
	for (i = 0; i != job->argc; ++i) {
		assert(TOKEN_TYPE_OF(token[offset]) == TOKEN_STR);
		job->argv[i] = token_str_transfer_string((token_str_t*) token[offset + i]);
	}
	job->argv[job->argc] = 0;
}

void job_destroy(job_t* job) {
	if (!job) return ;
	if (job->argv) {
		int i;
		for (i = 0; i != job->argc; ++i) {
			free(job->argv[i]);
		}
		free(job->argv);
	}

	free(job->in_file);
	free(job->out_file);

	free(job);
}

int dump_job(char* buffer, int buf_size, job_t* job) {
	int i;
	DUMP_START();
	DUMP(buffer, buf_size, "argc = %d", job->argc);
	if (job->argc > 0) {
		DUMP(buffer, buf_size, ", argv = (%s", job->argv[0]);
	}
	for (i = 1; i < job->argc; ++i) {
		DUMP(buffer, buf_size, ", %s", job->argv[i]);
	}
	DUMP(buffer, buf_size, "), use_pipe = %d, background = %d", job->use_pipe, job->background);
	if (job->in_file) DUMP(buffer, buf_size, ", in_file = %s", job->in_file);
	if (job->out_file) DUMP(buffer, buf_size, ", out_file = %s", job->out_file);
	DUMP_SUCCESS();
}

void job_list_clean(job_list_t* head) {
	while (head) {
		job_list_t* to_destroy = head;
		head = head->next;
		job_destroy(to_destroy->job);
		free(to_destroy);
	}
}

job_lists_t* new_job_lists(job_list_t* job_list, job_lists_t* next, pid_t last_pid) {
	job_lists_t* lists = malloc(sizeof(job_lists_t));
	if (!next)
		lists->num = 1;
	else
		lists->num = next->num + 1;
	lists->job_list = job_list;
	lists->next = next;
	lists->last_pid = last_pid;
	return lists;
}


