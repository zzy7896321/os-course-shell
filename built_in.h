#ifndef OS_COURSE_SHELL_BUILT_IN_H
#define OS_COURSE_SHELL_BUILT_IN_H

#define DECL_BUILTIN(name)	\
	int os_course_shell_built_in_##name(int argc, char* argv[], const char* in_file, const char* out_file)

#define CALL_BUILTIN(name, argc, argv, in, out)	\
	os_course_shell_built_in_##name(argc, argv, in, out)

DECL_BUILTIN(cd);

#endif

