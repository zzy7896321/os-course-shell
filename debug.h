#ifndef OS_COURSE_DEBUG_H
#define OS_COURSE_DEBUG_H

#include "config.h"
#include <stdio.h>

#ifndef NDEBUG
#define DEBUG_OUTPUT(fmt, ...) \
	fprintf(stderr, "%s(%d): " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define DEBUG_OUTPUT(...)	
#endif

/* dump facilities */

/**
 * DUMP_START_VAR_NAME should be called before any call to the subsequent DUMP facilities.
 * all calls should specify the same num_written variable.
 */
#define DUMP_START_VAR_NAME(num_written)	\
	int num_written = 0

/**
 * func should have the prototype:
 * int func(char* buffer, int buf_size, <at least one parameter here> ); 
 *
 * func should return the number of characters written on success (not including the terminating null-byte),
 * or negative value if an error occured. If the written content gets truncated due to buf_size limit, func
 * should return the number of characters that would be written if no limit was imposed.
 *
 * If func fails, this macro returns immediately with the same error number.
 */
#define DUMP_CALL_VAR_NAME(num_written, func, buffer, buf_size, ...)	\
	do {	\
		int ret = func(buffer + num_written, (buf_size >= num_written) ? (buf_size - num_written) : 0, __VA_ARGS__);	\
		if (ret < 0) return ret;	\
		num_written += ret;	\
	} while (0)


/**
 * DUMP with snprintf. Equivalent to DUMP_CALL_VAR_NAME(num_written, snprintf, buffer, buf_size, ...).
 */
#define DUMP_VAR_NAME(num_written, buffer, buf_size, ...)	\
	DUMP_CALL_VAR_NAME(num_written, snprintf, buffer, buf_size, __VA_ARGS__)

/**
 * Returns num_written.
 */
#define DUMP_SUCCESS_VAR_NAME(num_written)	\
	return num_written


/**
 * Returns with error.
 */
#define DUMP_ERROR_VAR_NAME(num_written, error_value)	\
	return error_value


/* the following DUMP uses the default name of num_written */
#define DUMP_START() DUMP_START_VAR_NAME(num_written)
#define DUMP_CALL(func, buffer, buf_size, ...) DUMP_CALL_VAR_NAME(num_written, func, buffer, buf_size, __VA_ARGS__)
#define DUMP(buffer, buf_size, ...) DUMP_VAR_NAME(num_written, buffer, buf_size, __VA_ARGS__)
#define DUMP_SUCCESS() DUMP_SUCCESS_VAR_NAME(num_written)
#define DUMP_ERROR(error_value) DUMP_ERROR_VAR_NAME(num_written, error_value)

extern BOOL debug_flags[256];
void init_debug_flags(CONST char* flags);
#define test_debug_flag(flag) (debug_flags[(int) (flag)])

/**
 *	Print the error message to stderr. 
 */
void report_error(CONST char* fmt, ...);

/**
 *	Print the error message to stderr and halt.
 */
void report_fatal_error(int exitcode, CONST char* fmt, ...);

#define os_shell_static_assert(condition)	\
	void os_shell_static_assert_failure(int v[][ (condition) ? 1 : -1 ])

#endif

