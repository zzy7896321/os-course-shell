#ifndef OS_COURSE_SHELL_TOKEN_H
#define OS_COURSE_SHELL_TOKEN_H

#include "config.h"

#include <stddef.h>

typedef enum token_type_t {
	TOKEN_INVALID = 0,
	TOKEN_REDIR_IN,
	TOKEN_REDIR_OUT,
	TOKEN_PIPE_IO,
	TOKEN_BACKGROUND,
	TOKEN_STR,
	TOKEN_EOLN,
	TOKEN_EOF,

	NUMBER_OF_TOKEN
} token_type_t;

extern CONST char* token_names[NUMBER_OF_TOKEN];

typedef struct token_t {
	token_type_t token_type;
} token_t;

typedef struct token_str_t /* extends token_t */ {
	token_t super;
	char* content;
} token_str_t;

/**
 *	Constructs a new token of type.
 */
token_t* new_token(token_type_t type);

/**
 *	Constructs a new string token. The ownership of str is transfered to the token.
 */
token_t* new_token_str_transfer(char* str);

/**
 *	Constructs a new string token. str[offset..offset + len] is copied.
 */
token_t* new_token_str_duplicate(CONST char* str, int offset, size_t len);

/**
 *	Transfer the string out of token.
 */
char* token_str_transfer_string(token_str_t* token);

/**
 *	Destroy the token of all types.
 */
void token_destroy(token_t* token);

/**
 *	Destroy the first length token in the array and sets them to NULL.
 */
void token_destroy_arr(token_t* token_arr[], size_t length);

#define TOKEN_TYPE_OF(token) (((token_t*) (token))->token_type)
#define TOKEN_STR_OF(token) (((token_str_t*) (token))->content)

/**
 *	Dump the token.
 *
 *	@returns	the number of characters written to the buffer, or -1 on failure,
 *				if the token get truncated due to buffer size limit, the return 
 *				value is the number of characters to be written as if there were
 *				no buffer size limit. (not including \0)
 */
int dump_token(char* buffer, int buf_size, token_t* token);

#endif

