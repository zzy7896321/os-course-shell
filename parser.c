#include "parser.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "debug.h"

token_t** parse_result = 0;
size_t parse_result_capacity = 0;
size_t parse_length = 0;
FILE* parse_input = 0;	// cannot initialize to stdin

os_shell_static_assert( PARSE_EOLN < 0 && PARSE_EOLN != EOF);

/* buffer of each line */
os_shell_static_assert( MAX_LINE_LENGTH >= 1 );
#define PARSE_INPUT_BUFFER_SIZE (MAX_LINE_LENGTH + 1)
static char parse_input_buffers[1][PARSE_INPUT_BUFFER_SIZE] = {{0}};

static char *parse_current_buffer = parse_input_buffers[0],
			*parse_current_buffer_cursor = parse_input_buffers[0];

static BOOL parse_EOF_reached = FALSE;

#define PARSE_CLEAR_BUFFER(buffer) buffer[0] = '\0'

int parse_reload_current_buffer() {
	PARSE_CLEAR_BUFFER(parse_current_buffer);

	/* load current buffer */
	PARSE_CLEAR_BUFFER(parse_current_buffer);
	parse_current_buffer_cursor = parse_current_buffer;
	if (!fgets(parse_current_buffer, PARSE_INPUT_BUFFER_SIZE, parse_input)) {
		if (feof(parse_input)) {
			parse_EOF_reached = TRUE;
			return EOF;
		}
		else { 
			report_fatal_error(OS_SHELL_IO_ERROR, "I/O error is encountered");
		}
	}

	return 0;
}

/**
 * Peek a character from the input. This operation may block.
 *
 * @returns	the next charater from the input without cursor advanced, or EOF if end-of-file encountered
 */
int parse_peek() {
	if (parse_EOF_reached) return EOF;

	assert(parse_current_buffer_cursor < parse_current_buffer + PARSE_INPUT_BUFFER_SIZE);
	if (*parse_current_buffer_cursor == '\0') {
		if (parse_reload_current_buffer() == EOF) return EOF;	
	}
	
	return (int) parse_current_buffer_cursor[0];
}

/**
 *	Gets a character from parse_input. This operation may block.
 *	@returns	the next character with cursor advanced by 1, or EOF if end-of-file encountered
 */
int parse_getc() {
	if (parse_EOF_reached) return EOF;

	assert(parse_current_buffer_cursor < parse_current_buffer + PARSE_INPUT_BUFFER_SIZE);
	if (*parse_current_buffer_cursor == '\0') {
		if (parse_reload_current_buffer() == EOF) return EOF;
	}
	
	DEBUG_OUTPUT("parse_getc(): (%d)%c", *parse_current_buffer_cursor, (char) *parse_current_buffer_cursor);

	return *parse_current_buffer_cursor++;
}

/**
 *	Peeks a character from parse_input. This operation never blocks.
 *	It may, however, fail when end-of-line is reached. Preceeding EOLN
 *	'\n' may or may not appear.
 *
 *	@returns	the next character on success, EOF if end-of-file is encountered,
 *				or PARSE_EOLN if end-of-line is encountered
 */
int parse_peek_nonblock() {
	if (parse_EOF_reached) return EOF;

	assert(parse_current_buffer_cursor < parse_current_buffer + PARSE_INPUT_BUFFER_SIZE);
	if (parse_current_buffer_cursor[0] == '\0') {
		char prev;
		assert(parse_current_buffer_cursor > parse_current_buffer);
		prev = parse_current_buffer_cursor[-1];
		if ( prev == '\n' || prev == '\r' ) return PARSE_EOLN;

		if (parse_reload_current_buffer() == EOF) return EOF;
	}

	return parse_current_buffer_cursor[0];
}

#define parse_is_blank(ch) isblank(ch)

static BOOL is_new_token[128] = {
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, TRUE , TRUE , TRUE , TRUE , TRUE , FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	TRUE , FALSE, FALSE, FALSE, FALSE, FALSE, TRUE , FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, TRUE , FALSE, TRUE , FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
	FALSE, FALSE, FALSE, FALSE, TRUE , FALSE, FALSE, FALSE, 
};
BOOL parse_is_new_token(int ch) {
	return ch < 0 || (ch < 128 && is_new_token[ch]);
}

os_shell_static_assert(MAX_TOKEN_LENGTH >= 1);
static char* token_buffer = 0;
static size_t token_buffer_size = 0;

token_t* parse_next_token() {
	#define INCREASE_BUFFER_SIZE()	\
		do {	\
			size_t new_token_buffer_size = token_buffer_size << 1;	\
			char* new_token_buffer = malloc(sizeof(char) * (new_token_buffer_size + 1));	\
			if (!new_token_buffer) {	\
				report_error("out of memory, token is too long");	\
				while (!parse_is_new_token(parse_peek_nonblock())) {	\
					parse_getc(); /*abandon the remaining of the token */	\
				}	\
				*buf_ptr++ = '\0';	\
				return new_token_str_duplicate(token_buffer, 0, buf_ptr - token_buffer);	\
			}	\
			else {	\
				strncpy(new_token_buffer, token_buffer, token_buffer_size + 1);	\
				buf_ptr = new_token_buffer + (buf_ptr - token_buffer);	\
				free(token_buffer);	\
				token_buffer = new_token_buffer;	\
				token_buffer_size = new_token_buffer_size;	\
			}	\
		} while(0)

	#define PUT_TO_BUFFER_CHECKED(ch)	\
		do {	\
			if (buf_ptr - token_buffer >= token_buffer_size) {	\
				INCREASE_BUFFER_SIZE();		\
			}	\
			*buf_ptr++ = ch;\
		} while (0)

	char* buf_ptr = token_buffer;
	while (TRUE) {
		int current = parse_getc();

		switch (current) {
		case EOF: 
			return new_token(TOKEN_EOF);
		case ' ': case '\t':
			continue;
		case '\n': case PARSE_EOLN:
			return new_token(TOKEN_EOLN);
		case '\r':
			if (parse_peek_nonblock() == '\n') {
				parse_getc();
			}
			return new_token(TOKEN_EOLN);
		case '<':
			return new_token(TOKEN_REDIR_IN);
		case '>':
			return new_token(TOKEN_REDIR_OUT);
		case '|':
			return new_token(TOKEN_PIPE_IO);
		case '&':
			return new_token(TOKEN_BACKGROUND);

		#define READ_QUOTED_STRING(quote_mark)	\
			do {	\
				while (TRUE) {	\
					current = parse_getc();	\
					if (current == '\\') {	\
						current = parse_getc();	\
						if (current == quote_mark || current == '\\') {	\
							PUT_TO_BUFFER_CHECKED(current);	\
							continue;	\
						}	\
						if (current == EOF) {	\
							report_error("syntax error: unexpected EOF, expecting %c", quote_mark);	\
							return 0;	\
						}	\
						PUT_TO_BUFFER_CHECKED('\\');	\
					}	\
					switch (current) {	\
					case quote_mark:	\
						*buf_ptr++ = '\0';	\
						return new_token_str_duplicate(token_buffer, 0, buf_ptr - token_buffer);	\
					case EOF:	\
						report_error("syntax error: unexpected EOF, expecting %c", quote_mark);	\
						return 0;	\
					default:	\
						PUT_TO_BUFFER_CHECKED(current);	\
					}	\
				}	\
				assert(FALSE);	\
			} while (0)
		case '\'':
			READ_QUOTED_STRING('\'');
			break;
		case '"':
			READ_QUOTED_STRING('"');
			break;

		#undef READ_QUOTED_STRING

		default:
			*buf_ptr++ = (char) current;
			while (!parse_is_new_token(parse_peek_nonblock())) {
				PUT_TO_BUFFER_CHECKED((char) parse_getc());
			}
			*buf_ptr++ = '\0';
			return new_token_str_duplicate(token_buffer, 0, buf_ptr - token_buffer);	
		}
	}

	#undef PUT_TO_BUFFER_CHECKED
	#undef INCREASE_BUFFER_SIZE

	assert(FALSE);
	return 0;
}

BOOL parse_next_line() {
	os_shell_static_assert( MAX_TOKEN_NUMBER >= 1 );
	if (!parse_result) {
		parse_result = malloc(sizeof(token_t*) * MAX_TOKEN_NUMBER);
		if (!parse_result) {
			report_fatal_error(OS_SHELL_OUT_OF_MEMORY, "cannot allocate space for parse_result");
		}

		memset(parse_result, 0, sizeof(token_t*) * MAX_TOKEN_NUMBER);
		parse_result_capacity = MAX_TOKEN_NUMBER;
	}

	else {
		token_destroy_arr(parse_result, parse_length);
	}
	parse_length = 0;
	
	#define INCREASE_PARSE_RESULT_CAPACITY()	\
		do {	\
			size_t new_parse_result_capacity = parse_result_capacity << 1;	\
			token_t** new_parse_result = malloc(sizeof(token_t*) * new_parse_result_capacity);	\
			if (!new_parse_result) {	\
				report_fatal_error(OS_SHELL_OUT_OF_MEMORY, "cannot increase space for parse_result");	\
			}	\
			memcpy(new_parse_result, parse_result, sizeof(token_t*) * parse_result_capacity);	\
			memset(new_parse_result + sizeof(token_t*) * parse_result_capacity, 0, 	\
					sizeof(token_t*) * (new_parse_result_capacity - parse_result_capacity));	\
			free(parse_result);	\
			parse_result = new_parse_result;	\
			parse_result_capacity = new_parse_result_capacity;	\
		} while(0)

	while (TRUE) {
		token_t* token = parse_next_token();
		if (!token) return FALSE;
		
		if (parse_length >= parse_result_capacity) {
			INCREASE_PARSE_RESULT_CAPACITY();
		}
		parse_result[parse_length++] = token;

		if (TOKEN_TYPE_OF(token) == TOKEN_EOLN || TOKEN_TYPE_OF(token) == TOKEN_EOF) {
			return TRUE;
		}
	}

	#undef INCREASE_PARSE_RESULT_CAPACITY
	assert(FALSE);
	return FALSE;
}

int parse_init(FILE* file) {
	parse_input = file;
	parse_EOF_reached = FALSE;

	parse_input_buffers[0][0] = '\0';
	parse_current_buffer = parse_current_buffer_cursor = parse_input_buffers[0];

	if (!token_buffer) {
		token_buffer = malloc(sizeof(char) * (MAX_TOKEN_LENGTH + 1));
		token_buffer_size = MAX_TOKEN_LENGTH;
	}

	return 1;
}


