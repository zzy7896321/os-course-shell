#include "debug.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

BOOL debug_flags[256];

void init_debug_flags(CONST char* flags) {
	memset(debug_flags, 0, sizeof(BOOL) * 256);
	if (!flags) return ;

	while (*flags) {
		debug_flags[(unsigned char) (*flags++)] = TRUE;
	}
}

void report_error(CONST char* fmt, ...) {
	fflush(stderr);
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "error: ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
	va_end(args);
}

void report_fatal_error(int exitcode, CONST char* fmt, ...) {
	fflush(stderr);
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "error: ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
	va_end(args);

	exit(exitcode);
}


